import React, { useState, useEffect } from 'react';

function PresentationForm() {
    const [conferences, setConferences] = useState([]);
    const [formData, setFormData] = useState({
        presenterName: '',
        presenterEmail: '',
        companyName: '',
        title: '',
        synopsis: '',
        conferenceId: ''
    });

    useEffect(() => {
        const loadConferences = async () => {
            const url = 'http://localhost:8000/api/conferences/';
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                setConferences(data.conferences);
            }
        };

        loadConferences();
    }, []);

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;

        setFormData({
            ...formData,
            [inputName]: value
        });
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        const json = JSON.stringify(formData);
        const locationUrl = `http://localhost:8000/api/conferences/${formData.conferenceId}/presentations/`;
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            setFormData({
                presenterName: '',
                presenterEmail: '',
                companyName: '',
                title: '',
                synopsis: '',
                conferenceId: ''
            });
        }
    };

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new presentation</h1>
                    <form onSubmit={handleSubmit} id="create-presentation-form">
                        {/* Form Fields */}
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} placeholder="Presenter Name" required type="text" name="presenterName" id="presenterName" className="form-control" />
                            <label htmlFor="presenterName">Presenter Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} placeholder="Presenter Email" required type="email" name="presenterEmail" id="presenterEmail" className="form-control" />
                            <label htmlFor="presenterEmail">Presenter Email</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} placeholder="Company Name" type="text" name="companyName" id="companyName" className="form-control" />
                            <label htmlFor="companyName">Company Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} placeholder="Title" required type="text" name="title" id="title" className="form-control" />
                            <label htmlFor="title">Title</label>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="synopsis">Synopsis</label>
                            <textarea onChange={handleFormChange} placeholder="Synopsis" id="synopsis" rows="3" name="synopsis" className="form-control"></textarea>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleFormChange} required name="conferenceId" id="conference" className="form-select">
                                <option value="">Choose a conference</option>
                                {conferences.map(conference => (
                                    <option key={conference.id} value={conference.id}>{conference.name}</option>
                                ))}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default PresentationForm;
