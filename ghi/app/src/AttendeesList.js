function AttendeesList(props) {

  // Check if attendees is an array and is not undefined
  if (!Array.isArray(props.attendees)) {
    // Return a message or empty content if attendees is not an array
    return <div>No attendees data available.</div>;
  }

  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Name</th>
          <th>Conference</th>
        </tr>
      </thead>
      <tbody>
        {props.attendees.map(attendee => {
          return (
            <tr key={attendee.href}>
              <td>{attendee.name}</td>
              <td>{attendee.conference}</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default AttendeesList;
