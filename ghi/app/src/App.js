import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Nav from './Nav';
import ConferenceForm from './ConferenceForm'; // Import your ConferenceForm component
import AttendConferenceForm from './AttendConferenceForm'; // Import your AttendConferenceForm component
import LocationForm from './LocationForm'; // Import your LocationForm component
import AttendeesList from './AttendeesList'; // Import your AttendeesList component
import PresentationForm from './PresentationForm';
import MainPage from './MainPage';

function App(props) {
  // Check if attendees data is undefined
  if (props.attendees === undefined) {
    return null; // Or you can render a loading indicator or a message
  }

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/conferences/new" element={<ConferenceForm />} />
          <Route path="/attendees/new" element={<AttendConferenceForm />} />
          <Route path="/locations/new" element={<LocationForm />} />
          <Route path="/attendees" element={<AttendeesList />} />
          <Route path="/presentations/new" element={<PresentationForm />} />
          <Route index element={<MainPage />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
