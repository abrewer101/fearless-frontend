import React, { useEffect, useState } from 'react';

function LocationForm() {
    const [states, setStates] = useState([]);
    const fetchData = async () => {
    const url = 'http://localhost:8000/api/states/';

    const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return(
    <p>A location form</p>
    );
}

export default LocationForm;

/////////////////////////////////////////

window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/states/';

    const statesResponse = await fetch(url);

    if (statesResponse.ok) {
        const data = await statesResponse.json();
        const selectTag = document.getElementById('state');
        for (let state of data.states) {
            const option = document.createElement('option');
            option.value = state.abbreviation;
            option.innerHTML = state.name;
            selectTag.appendChild(option);
        }
    }

    // Form submission handling code
    const formTag = document.getElementById('create-location-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault(); // Prevent the default form submission behavior

        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));


        const locationUrl = 'http://localhost:8000/api/locations/';
        const fetchConfig = {
            method: 'POST',
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const locationResponse = await fetch(locationUrl, fetchConfig);
        if (locationResponse.ok) {
            const newLocation = await locationResponse.json();

            formTag.reset(); // Reset the form after successful data submission
        }
    });
});
