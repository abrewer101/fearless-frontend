document.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/locations/';

    const response = await fetch(url);

    if (response.ok) {
        const data = await response.json();
        const selectTag = document.getElementById('location');
        for (let location of data.locations) {
            const option = document.createElement('option');
            option.value = location.id;
            option.innerHTML = location.name;
            selectTag.appendChild(option);
        }
        console.log("Locations loaded:", selectTag.innerHTML);
    }

    const form = document.getElementById('new-conference-form');
    form.addEventListener('submit', async (event) => {
        event.preventDefault();

        const formData = {
            name: document.getElementById('name').value,
            starts: document.getElementById('starts').value,
            ends: document.getElementById('ends').value,
            description: document.getElementById('description').value,
            max_presentations: parseInt(document.getElementById('max_presentations').value, 10),
            max_attendees: parseInt(document.getElementById('max_attendees').value, 10),
            location: parseInt(document.getElementById('location').value, 10),
        };

        console.log("Form data to submit:", formData);

        const locationUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                "Content-Type": "application/json",
            },
        };

        // const response = await fetch('http://localhost:8000/api/conferences/', {
        //     method: 'POST',
        //     headers: {'Content-Type': 'application/json'},
        //     body: JSON.stringify(formData),
        // });

        if (response.ok) {
            // Clear the form
            form.reset();
            // Additional success handling
        } else {
            // Handle errors
        }
    });
});
