import React, { useState, useEffect } from 'react';

function PresentationForm() {
    const [conferences, setConferences] = useState([]);
    const [formData, setFormData] = useState({
        presenterName: '',
        presenterEmail: '',
        companyName: '',
        title: '',
        synopsis: '',
        conferenceId: ''
    });

    useEffect(() => {
        const loadConferences = async () => {
            const url = 'http://localhost:8000/api/conferences/';
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                setConferences(data.conferences);
            }
        };
        loadConferences();
    }, []);

    const handleChange = (event) => {
        const { name, value } = event.target;
        setFormData(prevFormData => ({
            ...prevFormData,
            [name]: value
        }));
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        const json = JSON.stringify(formData);
        const locationUrl = `http://localhost:8000/api/conferences/${formData.conferenceId}/presentations/`;
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            setFormData({
                presenterName: '',
                presenterEmail: '',
                companyName: '',
                title: '',
                synopsis: '',
                conferenceId: ''
            });
            const newPresentation = await response.json();
            console.log(newPresentation);
        }
    };

    return (
        <form onSubmit={handleSubmit} id="create-presentation-form">
            <div className="form-group">
                <label htmlFor="presenterName">Presenter Name</label>
                <input type="text" className="form-control" id="presenterName" name="presenterName" value={formData.presenterName} onChange={handleChange} required />
            </div>
            <div className="form-group">
                <label htmlFor="presenterEmail">Presenter Email</label>
                <input type="email" className="form-control" id="presenterEmail" name="presenterEmail" value={formData.presenterEmail} onChange={handleChange} required />
            </div>
            <div className="form-group">
                <label htmlFor="companyName">Company Name</label>
                <input type="text" className="form-control" id="companyName" name="companyName" value={formData.companyName} onChange={handleChange} />
            </div>
            <div className="form-group">
                <label htmlFor="title">Title</label>
                <input type="text" className="form-control" id="title" name="title" value={formData.title} onChange={handleChange} required />
            </div>
            <div className="form-group">
                <label htmlFor="synopsis">Synopsis</label>
                <textarea className="form-control" id="synopsis" name="synopsis" rows="3" value={formData.synopsis} onChange={handleChange}></textarea>
            </div>
            <div className="form-group">
                <label htmlFor="conference">Conference</label>
                <select className="form-control" name="conferenceId" id="conference" value={formData.conferenceId} onChange={handleChange} required>
                    <option value="">Choose a conference</option>
                    {conferences.map(conference => (
                        <option key={conference.id} value={conference.id}>{conference.name}</option>
                    ))}
                </select>
            </div>
            <button type="submit" className="btn btn-primary">Create</button>
        </form>
    );
}

export default PresentationForm;
